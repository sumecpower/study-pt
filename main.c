#include <stdio.h>
#include <windows.h>
#include <time.h>
#include <stdlib.h>
#include <stdint.h>

#include "pt.h"

#define RAM_INTERNAL 0

static int gMid = -1;

static uint64_t getTime(void);

// task1 500ms打印一次
static int task1(void);

// task2 1s打印一次
static int task2(void);

// task3 按键a检测
static int task3(void);

// task3 按键b长按检测
static int task4(void);

int main() {
    while (1) {
        task1();
        task2();
        task3();
        task4();
    }
    return 0;
}

static uint64_t getTime(void) {
    struct timeval t;
    gettimeofday(&t, NULL);
    return t.tv_sec * 1000000 + t.tv_usec;
}

// task1 500ms打印一次
static int task1(void) {
    static struct pt pt;
    static uint64_t now;
    int ms;

    PT_BEGIN(&pt);

    now = getTime();
    ms = (int)(now / 1000);
    printf("%d task1\n", ms);
    PT_WAIT_UNTIL(&pt, getTime() - now > 500 * 1000);

    PT_END(&pt);
}

// task2 1s打印一次
static int task2(void) {
    static struct pt pt;
    static uint64_t now;
    int ms;

    PT_BEGIN(&pt);

    now = getTime();
    ms = (int)(now / 1000);
    printf("%d task2\n", ms);
    PT_WAIT_UNTIL(&pt, getTime() - now > 1000 * 1000);

    PT_END(&pt);
}

// task3 按键a检测
static int task3(void) {
    static struct pt pt;
    static uint64_t now;
    char ch;

    PT_BEGIN(&pt);

    now = getTime();
    PT_WAIT_UNTIL(&pt, getTime() - now > 20 * 1000);

    if (!_kbhit()) {
        PT_EXIT(&pt);
    }

    ch =_getch();
    if (ch != 'a') {
        PT_EXIT(&pt);
    }

    printf("a is press!\n");

    PT_END(&pt);
}

// task3 按键b长按检测
static int task4(void) {
    static struct pt pt;
    static uint64_t now;
    static uint64_t timeBegin;

    PT_BEGIN(&pt);

    now = getTime();
    PT_WAIT_UNTIL(&pt, getTime() - now > 20 * 1000);

    if (!_kbhit()) {
        PT_EXIT(&pt);
    }

    if (_getch() != 'b') {
        PT_EXIT(&pt);
    }

    timeBegin = getTime();
    PT_WAIT_UNTIL(&pt, _getch() != 'b' || getTime() - timeBegin > 2000 * 1000);
    if (_getch() != 'b') {
        PT_EXIT(&pt);
    }

    printf("b is long press!\n");

    PT_END(&pt);
}
